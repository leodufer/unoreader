/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service.impl;

import com.ideasagiles.reader.domain.PasswordRecovery;
import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.PasswordRecoveryRepository;
import com.ideasagiles.reader.repository.UserRepository;
import com.ideasagiles.reader.service.PasswordRecoveryService;
import java.util.Date;
import javax.mail.internet.MimeMessage;
import jodd.util.RandomStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PasswordRecoveryServiceImpl implements PasswordRecoveryService {

    @Autowired
    private PasswordRecoveryRepository passwordRecoveryRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void requestPasswordRecovery(String username) {
        User user = userRepository.getByUsername(username);
        if (user != null) {
            PasswordRecovery pr = new PasswordRecovery();
            pr.setUser(user);
            pr.setToken(RandomStringUtil.randomAlphaNumeric(50));
            pr.setCreatedOn(new Date());
            passwordRecoveryRepository.save(pr);
            sendPasswordRecoveryEmail(username, pr.getToken());
        }
        else {
            throw new EmptyResultDataAccessException(1);
        }
    }

    @Override
    public void resetPassword(String token, String newPassword) {
        PasswordRecovery pr = passwordRecoveryRepository.getByToken(token);
        if (pr != null) {
            pr.getUser().setPassword(passwordEncoder.encode(newPassword));
            passwordRecoveryRepository.delete(pr);
        }
        else {
            throw new EmptyResultDataAccessException(1);
        }
    }

    private void sendPasswordRecoveryEmail(final String email, final String token) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                message.setTo(email);
                message.setSubject("Password reset for Uno Reader");
                String url = "http://www.unoreader.com/passwordReset.html?token=" + token;
                String text = "<html><body><h4>You requested a password reset for Uno Reader.</h4>"
                        + "<p>Please visit the following URL to reset your password:</p>"
                        + "<p><a href='" + url + "'>" + url + "</a></p>"
                        + "<p><small>(if this doesn't make sense to you, just delete this email)</small></p>"
                        + "</body></html>";
                message.setText(text, true);
            }
        };
        this.mailSender.send(preparator);
    }
}
