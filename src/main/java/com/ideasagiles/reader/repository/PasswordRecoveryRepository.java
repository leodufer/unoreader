/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.repository;

import com.ideasagiles.reader.domain.PasswordRecovery;
import org.springframework.data.repository.CrudRepository;

public interface PasswordRecoveryRepository extends CrudRepository<PasswordRecovery, Long> {

    PasswordRecovery getByToken(String token);
    
}
