/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.controller;

import com.ideasagiles.reader.vo.PasswordRecoveryVo;
import com.ideasagiles.reader.service.PasswordRecoveryService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class PasswordRecoveryController {

    @Autowired
    private PasswordRecoveryService passwordRecoveryService;

    @RequestMapping(value = "/passwordRecovery/{username:.+}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void requestPasswordRecovery(@PathVariable String username) {
        passwordRecoveryService.requestPasswordRecovery(username);
    }

    @RequestMapping(value = "/passwordRecovery", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void resetPassword(@RequestBody PasswordRecoveryVo passwordRecoveryVo) {
        passwordRecoveryService.resetPassword(passwordRecoveryVo.getToken(), passwordRecoveryVo.getPassword());
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody String userOrTokenNotFound(Exception ex, HttpServletRequest request) {
        return "The username or the token are not valid.";
    }
}
