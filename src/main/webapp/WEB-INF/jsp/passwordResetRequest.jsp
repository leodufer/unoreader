<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="container">
    <div class="row">
        <div class="span6 offset3">
            <div class="ur-lightbox ur-extra-margin">
                <div class="ur-header"><h3>Enter your email to reset your password</h3></div>
                <form id="requestPasswordResetForm">
                    <div class="ur-content ur-extra-padding">
                        <div id="infoArea"></div>
                        <label>Email</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="email" class="input-xlarge" name="username" placeholder="Your email address..." required="required" maxlength="50" />
                        </div>
                        <p>
                            An email will be sent to your email address with instructions to reset your password.
                        </p>
                    </div>
                    <div class="ur-actions">
                        <input id="okButton" class="btn btn-success" type="submit" value="Reset Password"/>
                        <a class="btn" href="<c:url value="/login.html"/>">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!--********************************************************************
                            TEMPLATES
*********************************************************************-->
<script id="messageTemplate" type="text/x-jsrender">
    <div class="alert {{:cssClass}}">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>{{:title}}</h4>
        {{:text}}
    </div>
</script>