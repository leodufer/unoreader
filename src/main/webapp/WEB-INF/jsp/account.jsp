<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="container">
    <div class="row">
        <div class="span6 offset3">
            <div class="ur-lightbox ur-extra-margin">
                <div class="ur-header"><h3>Your account</h3></div>
                <form id="accountForm">
                    <div class="ur-content ur-extra-padding">
                        <div id="infoArea"></div>
                        <label>Email</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="email" class="input-xlarge" name="username" placeholder="Your email address..." required="required" maxlength="50" value="<sec:authentication property="principal.username"/>"/>
                        </div>
                        <hr/>
                        <label>Current Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="oldPassword" placeholder="Type your old password..." pattern=".{5,25}" />
                        </div>
                        <label>New Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="newPassword1" placeholder="Type a new password..." pattern=".{5,25}" />
                        </div>
                        <label>Retype Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="newPassword2" placeholder="Retype the new password..." pattern=".{5,25}"/>
                        </div>
                    </div>
                    <div class="ur-actions">
                        <input class="btn btn-success" type="submit" value="Save Changes"/>
                        <a class="btn" href="<c:url value="/"/>">Cancel</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<!--********************************************************************
                            TEMPLATES
*********************************************************************-->
<script id="errorMessageTemplate" type="text/x-jsrender">
    <div class="alert {{:cssClass}}">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>{{:title}}</h4>
        {{:text}}
    </div>
</script>