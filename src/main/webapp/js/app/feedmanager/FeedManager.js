/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.FeedManager = function(feedList) {

    /* ************************************************************
     * Initialization.
     *************************************************************/
    var feeds;
    var channels;

    //Always return a new instances, even if called without 'new'
    if (!(this instanceof unoreader.FeedManager)) {
        return new unoreader.FeedManager(feedList);
    }

    init(feedList);

    /* Initialization actions for this instance. */
    function init(feedList) {
        feeds = feedList;
        channels = filterChannelsFromFeeds(feeds);
        feeds.sort(sorterByName);
    }


    /* ************************************************************
     * Public functions.
     *************************************************************/

    function getChannels() {
        return channels;
    }

    /** Returns all distinct feeds (comparing its url) from all channels. */
    function findAllDistinctFeeds() {
        var uniqueFeeds;
        uniqueFeeds = filterByUniqueAttribute(feeds, "url");
        uniqueFeeds.sort(sorterByName);
        return uniqueFeeds;
    }

    function findFeedsByChannel(channel) {
        return feeds.filter(function(feed) {
            return feed.channel === channel;
        });
    }

    function findFeedById(id) {
        var list = feeds.filter(function(feed) {
            return feed.id === id;
        });
        return list[0] ? list[0] : null;
    }

    /**
     * Find feeds entries using an optional filter.
     * @param {Object} options filter and callbacks. feedId and channelId are
     * filters for the feeds. If no filter is specified, all feeds from all
     * channels are returned. Possible options attributes:
     *   feedId: entries only from the given feedId.
     *   channel: entries from all feeds in the given channel.
     *   maxEntriesPerFeed: max number of entries to retrieve per feed.
     *   success: a success callback. The callback is called with
     * a unoreader.PageIterator object that contains the entries.
     *   error: an optional error callback. The callback is called with the
     * result of the invocation.
     *   complete: an optional always callback. The callback is called after
     * the success or error callback.
     */
    function loadFeedEntries(options) {
        var feedList;
        var feed;
        var entries = [];

        //prepare feedList
        if (options && options.feedId) {
            feed = findFeedById(options.feedId);
            feedList = [];
            if (feed) {
                feedList[0] = feed;
            }
        }
        else if (options && options.channel) {
            feedList = findFeedsByChannel(options.channel);
        }
        else {
            feedList = findAllDistinctFeeds();
        }

        //perform async search
        async.each(feedList, function(feed, callback) {
            unoreader.webreader.loadFeed({
                url: feed.url,
                includeHistorialEntries: true,
                numEntries: options.maxEntriesPerFeed,
                success: function(result) {
                    entries = entries.concat(result.feed.entries);
                    callback();
                },
                error: function(result) {
                    if (options.error) {
                        callback(result);
                    }
                    else {
                        callback();
                    }
                }
            });
        }, function(error) {
            //load is finished
            if (error && options.error) {
                options.error(error);
            }
            else {
                entries.sort(sorterByPublishedDate);
                options.success(new unoreader.PageIterator(entries));
            }
            if (options.complete) {
                options.complete();
            }
        });
    }


    /* ************************************************************
     * Filters.
     *************************************************************/

    /** Prepares the channel list from the feed list.
     * @param {Array} feeds an Array of feeds to look for channels.
     */
    function filterChannelsFromFeeds(feeds) {
        var uniqueChannels;
        uniqueChannels = feeds.map(function(o) {
            return o.channel;
        });
        uniqueChannels = _.uniq(uniqueChannels);
        uniqueChannels.sort();
        return uniqueChannels;
    }

    /** Filters an array an returns a new array with unique elements, comparing
     *  the attribute "attributeName" of each element.
     * @param {Array} array an array that contains objects with an "attributeName" attribute.
     * @param {String} attributeName the name of the attribute to compare for uniqueness.
     * @return {Array} an array elements with unique "attributeName" attributes.
     */
    function filterByUniqueAttribute(array, attributeName) {
        return _.uniq(array, false, function(element) {
            return element[attributeName];
        });
    }


    /* ************************************************************
     * Sorters.
     *************************************************************/

    function sorterByName(a, b) {
        return a.name <= b.name ? -1 : 1;
    }

    function sorterByPublishedDate(a, b) {
        var dateFormat = unoreader.webreader.getDateFormatPattern();
        var dateA = moment(a.publishedDate, dateFormat);
        var dateB = moment(b.publishedDate, dateFormat);
        if (dateA && dateB) {
            return dateA.isBefore(dateB) ? 1 : -1;
        }
        else {
            return 0;
        }
    }


    /* ************************************************************
     * Public object attributes.
     *************************************************************/

    this.getChannels = getChannels;
    this.findFeedsByChannel = findFeedsByChannel;
    this.findAllDistinctFeeds = findAllDistinctFeeds;
    this.findFeedById = findFeedById;
    this.loadFeedEntries = loadFeedEntries;

};