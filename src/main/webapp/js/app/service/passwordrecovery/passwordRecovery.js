/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.service.passwordRecovery = (function() {
    function requestPasswordReset(username, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/passwordRecovery/" + encodeURIComponent(username);
        unoreader.service.postJSON({
            url: url,
            success: successCallback,
            error: errorCallback
        });
    }

    function resetPassword(resetPassword, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/passwordRecovery";
        unoreader.service.putJSON({
            url: url,
            data: resetPassword,
            success: successCallback,
            error: errorCallback
        });
    }

    return {
        requestPasswordReset: requestPasswordReset,
        resetPassword: resetPassword
    };
})();