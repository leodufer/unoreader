/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var page = (function() {
    function init() {
        var ENTRIES_DATE_FORMAT = "ddd[,] D MMMM YYYY HH:mm";
        $.views.converters({
            formatDate: function(val) {
                return moment(val).format(ENTRIES_DATE_FORMAT);
            }
        });

        unoreader.service.user.findAll(renderUsers);
    }

    function renderUsers(users) {
        $("#usersList").html($("#usersListTemplate").render(users));
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    page.init();
});