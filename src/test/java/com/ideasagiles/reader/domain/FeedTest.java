/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.domain;

import org.junit.Test;
import static org.junit.Assert.*;

public class FeedTest {

    @Test
    public void setChannel_nameWithSpaces_trimsAndLowercasesName() {
        String channel = "  a ChaNNel   ";
        Feed feed = new Feed();
        feed.setChannel(channel);
        assertEquals(channel.trim().toLowerCase(), feed.getChannel());
    }

    @Test(expected = NullPointerException.class)
    public void setChannel_withNull_throwsException() {
        Feed feed = new Feed();
        feed.setChannel(null);
    }

}
